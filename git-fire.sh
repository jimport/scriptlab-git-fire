#!/bin/bash

slab_exec() {
  runSU=$1
  eval "$SLAB_DIR/util/exec.sh $DEBUG $runSU 0 $2"
}

load_config() {

  DEBUG=$( sh $SLAB_DIR/util/conf_loader.sh DEBUG $SLAB_DIR/scriptlab.conf )
}

check_git() {
  local call_dir=$(pwd)
  local GIT_DIR=""
  while [ $call_dir != "/" ]
  do
    if [ -e $call_dir/.git ]
    then
      GIT_DIR="$call_dir"
    fi
    call_dir=$(dirname "$call_dir")
  done 
  echo "$GIT_DIR"
}

emergency_push() {

  # TODO: create log file on this event

  if [ ! -z "$( cd $1 && git status --porcelain)" ]
  then
    echo " * [PUSH] $1"
    slab_exec 1 "cd $1 && git checkout -b fire_$timestamp"
    slab_exec 1 "cd $1 && git add ."
    slab_exec 1 "cd $1 && git commit -m 'FIRE! FIRE!'"
    slab_exec 1 "cd $1 && git push origin fire_$timestamp"
  else
    echo " * [OK]   $1"
  fi
}

gf_usage() {
  echo "[ Usage ]"
  echo "> git-fire [secure|unsecure]"
}

gf_secure() {
  repo_path=`check_git`
  if [ ! -z "$( grep -x $repo_path $SCRIPT_DIR/repos.dat )" ]
  then
    echo " * Already protected!"
    exit 1
  fi

  echo " * Protecting : $repo_path"
  if [ ! -z "$repo_path" ]
  then
    echo "$repo_path" >> $SCRIPT_DIR/repos.dat
  else
    echo " * I'm sorry Dave, I can't do that"
  fi
}

gf_unsecure() {

  repo_path=`check_git`
  duplicate_regex="'^$repo_path$'"
  if [ ! -z "$( grep -x $repo_path $SCRIPT_DIR/repos.dat )" ]
  then
    echo " * Removing : $repo_path"
    slab_exec 1 "cp $SCRIPT_DIR/repos.dat $SCRIPT_DIR/repos.bak"
    slab_exec 1 "grep -v $duplicate_regex $SCRIPT_DIR/repos.bak > $SCRIPT_DIR/repos.dat" 
  else 
    echo " * Not protected!"
  fi
}

gf_run() {
  repo_path=`check_git`
  
  if [ $( wc -l < $SCRIPT_DIR/repos.dat ) -gt 0 ]
  then
    readarray -t a < $SCRIPT_DIR/repos.dat
    for i in ${a[@]}
    do
      emergency_push ${i}
    done

  elif [ ! -z "$repo_path" ]
  then
    emergency_push $repo_path
  else
    echo " * I'm sorry Dave, I can't do that"
  fi
}

#
# MAIN
#

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SLAB_DIR="$SCRIPT_DIR/../.."

load_config

# ---- START CODING HERE ----

timestamp=`date +%Y%m%d-%H%M`

case $1 in

  secure)
    gf_secure
    ;;

  unsecure)
    gf_unsecure
    ;;

  list)
    cat $SCRIPT_DIR/repos.dat
    ;;

  *)
    gf_run
    ;;
esac

